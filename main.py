import asyncio

from aiogram import Bot, Dispatcher, executor
from aiogram.types import Message

from currency_parse import selenium_parse_currency
from register_users import UserManager
from config import API_TOKEN, MONITORING_TIMER

user_manager = UserManager()


bot = Bot(API_TOKEN)
dp = Dispatcher(bot)


async def send_currency(chat_id, rates):
    if rates is None:
        await bot.send_message(chat_id, 'К сожалению, нет данных с обменников, попробуйте позже спросить 😺')
    else:
        await bot.send_message(chat_id, f'Лучшая покупка-продажа 💵 USD = {rates["usd_buy"][1]}-{rates["usd_sell"][1]}\n'
                                    f'Лучшая покупка-продажа 💶 Евро = {rates["euro_buy"][1]}-{rates["euro_sell"][1]}\n'
                                    f'Лучшая покупка-продажа 💷 Рубль = {rates["rub_buy"][1]}-{rates["rub_sell"][1]}\n')


@dp.message_handler(commands=['start', 'register'])
async def start(msg: Message):
    user_manager.register_user(msg.from_user.id)
    await msg.answer("Hi! I've just 📋✏ registered you!")


@dp.message_handler(commands=['stop', 'leave'])
async def start(msg: Message):
    user_manager.unregister_user(msg.from_user.id)
    await msg.answer("I've unregistered you. Good luck! 🤠")


@dp.message_handler(commands=['current', 'currency'])
async def currently(msg: Message):
    rates = selenium_parse_currency()
    await send_currency(msg.from_user.id, rates)


@dp.message_handler()
async def echo(msg: Message):
    await msg.answer("Hello!🤗\nI'm happy with your message! 🤠\n But, unfortunately now I am in development process.🛠    \nBest wishes 🙌")


async def monitoring_circle(num=0):
    if num > 1000:
        print('Достигнут лимит мониторинга')
    await asyncio.sleep(MONITORING_TIMER)
    best_rates = selenium_parse_currency()
    for user in user_manager.users:
        await send_currency(user, best_rates)

    asyncio.create_task(monitoring_circle(num+1))


async def on_startup(_):
    user_manager.read_users()
    asyncio.create_task(monitoring_circle())

if __name__ == '__main__':
    print('start')
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)

    print('after bot start')

