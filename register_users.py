from config import REGISTERED_USERS_FILE_PATH
import json
import os


class UserManager:
    def __init__(self):
        self.users = []

    def write_users(self):
        with open(REGISTERED_USERS_FILE_PATH, 'w', encoding='utf-8') as fout:
            json.dump(self.users, fout, ensure_ascii=False, indent=4)

    def register_user(self, chat_id):
        if chat_id not in self.users:
            self.users.append(chat_id)
            self.write_users()

    def unregister_user(self, chat_id):
        if chat_id in self.users:
            self.users.remove(chat_id)
            self.write_users()

    def read_users(self):
        if not os.path.exists(REGISTERED_USERS_FILE_PATH):
            self.users = []
        else:
            with open(REGISTERED_USERS_FILE_PATH) as fin:
                self.users = json.load(fin)
