import os

BASEDIR = os.path.abspath(os.path.dirname(__file__))

with open('api_token.txt', 'r', encoding='utf-8') as fin:
    API_TOKEN = fin.read().strip()

REGISTERED_USERS_FILE_PATH = 'users.json'
# MONITORING_TIMER = 1800  # = 30 minutes # seconds
MONITORING_TIMER = 300  # = 30 minutes # seconds
