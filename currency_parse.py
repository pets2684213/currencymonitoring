from selenium import webdriver
from selenium.webdriver.common.by import By

CURRENCY_SITE_URL = 'https://kurs.kz/index.php?mode=astana'


def selenium_parse_currency():
    driver = webdriver.Chrome()
    driver.get(CURRENCY_SITE_URL)
    # TODO: проверять только работающие пункты или все?
    elements = driver.find_elements(By.CLASS_NAME, 'punkt-open')
    # elements = driver.find_elements(By.CSS_SELECTOR, "table.table-striped.table-kurs.hovered-false>tbody>tr")
    elements = tuple(filter(lambda x: x.text, elements)) # фильтрую пустые строки
    if len(elements) == 0:
        return
    exchanges = []
    for el in elements:
        exchange = dict()

        info = el.find_element(By.TAG_NAME, 'td')
        exchange['name'] = info.find_element(By.TAG_NAME, 'a').text
        exchange['address'] = info.find_element(By.TAG_NAME, 'address').text

        currencies = el.find_elements(By.CLASS_NAME, 'currency')
        usd = currencies[0].text.split('—')
        euro = currencies[1].text.split('—')
        rub = currencies[2].text.split('—')
        exchange['usd_buy'] = usd[0]
        exchange['usd_sell'] = usd[1]
        exchange['euro_buy'] = euro[0]
        exchange['euro_sell'] = euro[1]
        exchange['rub_buy'] = rub[0]
        exchange['rub_sell'] = rub[1]

        exchanges.append(exchange)

    # Ищу лучшие курсы по валютам
    f = exchanges[0]
    best = {'usd_buy': [0, float(f['usd_buy'])], 'usd_sell': [0, float(f['usd_sell'])],
            'euro_buy': [0, float(f['euro_buy'])], 'euro_sell': [0, float(f['euro_sell'])],
            'rub_buy': [0, float(f['rub_buy'])], 'rub_sell': [0, float(f['rub_sell'])]}
    for i, exchange in enumerate(exchanges):
        try:
            usd_buy, usd_sell = float(exchange['usd_buy']), float(exchange['usd_sell'])
            euro_buy, euro_sell = float(exchange['euro_buy']), float(exchange['euro_sell'])
            rub_buy, rub_sell = float(exchange['rub_buy']), float(exchange['rub_sell'])

            if usd_buy > best['usd_buy'][1]:
                best['usd_buy'][0] = i
                best['usd_buy'][1] = usd_buy
            if euro_buy > best['euro_buy'][1]:
                best['euro_buy'][0] = i
                best['euro_buy'][1] = euro_buy
            if rub_buy > best['rub_buy'][1]:
                best['rub_buy'][0] = i
                best['rub_buy'][1] = rub_buy

            if usd_sell < best['usd_sell'][1]:
                best['usd_sell'][0] = i
                best['usd_sell'][1] = usd_sell
            if euro_sell < best['euro_sell'][1]:
                best['euro_sell'][0] = i
                best['euro_sell'][1] = euro_sell
            if rub_sell < best['rub_sell'][1]:
                best['rub_sell'][0] = i
                best['rub_sell'][1] = rub_sell
        except Exception as e:
            print('Expection:', e)

    return best


if __name__ == '__main__':
    selenium_parse_currency()
